import React, { Component } from "react";
import ReactDOM from "react-dom";
import { Grid, Row, Col } from "react-flexbox-grid";
import "./ContactCard.css";
import { ContactDetail } from "./ContactDetail";

const imgPlaceholder = require("./user_small.png");

const favoriteStarImg = {
  positive: require("./favorite_true_small.png"),
  negative: require("./favorite_false_small.png")
};

export class ContactCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      image: this.props.contact.smallImageURL
    }
  }

  openContact() {
    let contactDetail = <ContactDetail
    contact={this.props.contact}
    allContacts={this.props.allContacts} />;
    ReactDOM.render(contactDetail, document.getElementById("root"));
  }

  render() {
    return (
      <div className="ContactCard" onClick={this.openContact.bind(this)}>
        <Grid>
          <Row>
            <Col xs={2}>
              <img
                src={this.state.image}
                className={"contact-card"}
                onError={() => {
                  this.setState({ image: imgPlaceholder });
                }}
              />
            </Col>

            <Col xs={2}>
              <img
                src={
                  this.props.contact.isFavorite
                    ? favoriteStarImg.positive
                    : null
                }
                className={"small-star"}
              />
            </Col>

            <Col xs={8}>
              <Row className="name"><Col xs>{this.props.contact.name}</Col></Row>
              <Row className="company-name"><Col xs>{this.props.contact.companyName}</Col></Row>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}
