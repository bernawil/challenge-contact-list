import React, { Component } from "react";
import ReactDOM from 'react-dom';
import "./ContactDetail.css";
import { Grid, Row, Col } from "react-flexbox-grid";
import { ContactList } from "./ContactList"

const imgPlaceholder = require("./user_large.png");

const favoriteStarImg = {
  positive: require("./favorite_true_small.png"),
  negative: require("./favorite_false_small.png")
};

export class ContactDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allContacts: props.allContacts,
      contact: props.contact,
      image: props.contact.largeImageURL
    };
  }

  componentDidMount() {
    this.setState((prevState, props) => {
      image: this.props.contact.largeImageURL;
    });
  }

  getDisplayAddress() {
    return Object.values(this.props.contact.address).reduce(
      (accu, curr) => accu + ", " + curr
    );
  }

  toggleFavoriteStatus() {
    this.setState((prevState, props) => {
      let c = prevState.contact
      c.isFavorite = !c.isFavorite
      let allC = prevState.allContacts.filter(element => element.id !== prevState.contact.id)
      allC.push(c)
      return {
        contact: c,
        allContacts: allC
      }
    })
  }

  returnToContactList() {
    const contactList = <ContactList contacts={this.state.allContacts} />
    ReactDOM.render(contactList , document.getElementById("root"));
  }

  render() {
    return (
      <div className="ContactDetail">
        <Grid className="header">
          <Row between={"xs"}>
            <Col xs={4} className={"back-btn"} >
              <span onClick={() => {this.returnToContactList()}}>{"< Contacts"}</span>
            </Col>
            <Col xs={2}>
              <img
                className={"star-status"}
                src={
                  this.state.contact.isFavorite
                    ? favoriteStarImg.positive
                    : favoriteStarImg.negative
                }
                onClick={() => { this.toggleFavoriteStatus()}}
                alt={"favorite contact status"}
              />
            </Col>
          </Row>
        </Grid>

        <Grid>
          <Row className="main-info">
            <Col center={"xs"} xs>
              <img
                alt={"contact"}
                src={this.state.image}
                className="contact-image"
                onError={() => {
                  this.setState({ image: imgPlaceholder });
                }}
              />
              <h1> {this.props.contact.name} </h1>
              <p> {this.props.contact.companyName} </p>
            </Col>
          </Row>

          <Row center={"xs"} className="secondary-info">
            <Col botton={"xs"} className="separator" />
          </Row>

          <SecondaryInfo
            title={"PHONE:"}
            info={this.props.contact.phone.home}
            subtitle={"home"}
          />

          <SecondaryInfo
            info={this.props.contact.phone.mobile}
            title={"PHONE:"}
            subtitle={"mobile"}
          />

          <SecondaryInfo
            info={this.props.contact.phone.work}
            title={"PHONE:"}
            subtitle={"work"}
          />

          <SecondaryInfo info={this.getDisplayAddress()} title={"ADDRESS:"} />

          <SecondaryInfo
            info={this.props.contact.birthdate}
            title={"BIRTHDATE:"}
          />

          <SecondaryInfo info={this.props.contact.email} title={"EMAIL:"} />
        </Grid>
      </div>
    );
  }
}

function SecondaryInfo(props) {
  if (!props.info) {
    return null;
  }

  let subtitle = null;
  if (props.subtitle) {
    subtitle = props.subtitle;
  }

  return (
    <Grid center={"xs"} className="secondary-info">
      <Row center={"xs"}>
        <Col xs>
          <Row center={"xs"}>
            <Col xs>
              <Row center={"xs"}>
                <Col className={"title"} xs>
                  {props.title}
                </Col>
              </Row>
              <Row center={"xs"}>
                <Col className={"info"} xs>
                  {props.info}
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
        <Col className={"subtitle"} xs>
          {subtitle}
        </Col>
      </Row>
      <Row center={"xs"} className="separator" />
    </Grid>
  );
}
