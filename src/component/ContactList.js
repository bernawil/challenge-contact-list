import React, { Component } from "react";
import { Grid, Row, Col } from "react-flexbox-grid";
import { ContactCard } from "./ContactCard";
import "./ContactList.css";

export class ContactList extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div className="ContactList">
        <div className="fav-separator">{"favorite contacts"}</div>
        <Grid>
          {this.props.contacts
            .filter(contact => contact.isFavorite)
            .map(contact => (
              <Row key={contact.id}>
                <ContactCard
                  imageUrl={
                    contact.smallImageURL ? contact.smallImageURL : null
                  }
                  contact={contact}
                  allContacts={this.props.contacts}
                />
              </Row>
            ))}
        </Grid>

        <div className="fav-separator">{"other contacts"}</div>

        <Grid>
          {this.props.contacts
            .filter(contact => !contact.isFavorite)
            .map(contact => (
              <Row key={contact.id}>
                <ContactCard
                  imageUrl={
                    contact.smallImageURL ? contact.smallImageURL : null
                  }
                  contact={contact}
                  allContacts={this.props.contacts}
                />
              </Row>
            ))}
        </Grid>
      </div>
    )
  }
}

ContactList.defaultProps = {
  contacts: []
}
