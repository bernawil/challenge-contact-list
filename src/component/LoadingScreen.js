import React, { Component } from "react";
import "./LoadingScreen.css";

export class LoadingScreen extends Component {
  render() {
    return (
      <div className="LoadingScreen">
       <p>{ 'Please wait, loading your contacts' }</p>
      </div>
    )
  }
}

