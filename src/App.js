import React, { Component } from "react";
import ReactDOM from 'react-dom';
import logo from "./logo.svg";
import "./App.css";
import { ContactList } from "./component/ContactList";
import { ContactDetail } from "./component/ContactDetail";
import { Grid, Row, Col } from "react-flexbox-grid";
import { LoadingScreen } from "./component/LoadingScreen.js";

const apiUrl = "https://s3.amazonaws.com/technical-challenge/v3/contacts.json";
const contactsMock = require("./component/apiMock.json")

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      contacts: this.fetchContacts()
    }
  }

  fetchContacts() {
    fetch(apiUrl, {
      mode: "no-cors"
    })
      .then(response => response.text())
      .catch(error => {
        console.log(error)
      })
      .then(text => {
        let json = null
        if (text)
          json = JSON.parse(text)
        else
          json = require("./component/apiMock.json")

        this.setState({
          contacts: json
        })

      })
  }

  render() {
    return (<div className="App">
      <ContactListHeader />
      <ContactList contacts={this.state.contacts} />
      </div>)
  }
}

App.defaultProps = {
  contacts: []
};

class ContactListHeader extends Component {

  render(){
    return (
      <div className="App-header">
      <h1>{"Contacts"}</h1>
      </div>
    )
  }
}

export default App;
